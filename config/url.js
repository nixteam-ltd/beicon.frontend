let host = 'https://www.beicon.ru/api';

if (process.env.NODE_ENV === 'development')
	host = 'https://testbeicon.it-sfera.ru/api';

const cfg = {

	articles : {
		url  : host + '/rarticles',
		tbl  : 'articles',
		name : 'Статьи'
	},
	category : {
		url  : host + '/rsections',
		tbl  : 'sections',
		name : 'Рубрики'
	},
	sections : {
		url  : host + '/categories',
		tbl  : 'category',
		name : 'Разделы'
	},
	persons : {
		url  : host + '/persons',
		tbl  : 'persons',
		name : 'Персоны'
	},
	tags : {
		url  : host + '/rtags',
		tbl  : 'tags',
		name : 'Теги'
	},
	sitepages : {
		url  : host + '/pages',
		tbl  : 'pages',
		name : 'Страницы'
	},


	auth               : host + '/login',
	gallery            : host + '/galleries',
	galleryitems       : host + '/galleryitems',
	gallerysort        : host + '/gis',
	getSectCategories  : host + '/catSections',
	notifications      : host + '/notifies',
	pnews              : host + '/pnews',
	recArticles        : host + '/recomended',
	rss                : host + '/rsses',
	rssArticles        : host + '/articlerss',
	saveSectCategories : host + '/catupdate',
	sectCategories     : host + '/catSections',
	service            : host + '/metas',
	subscribers        : host + '/subscribers',
	upload             : host + '/image_upload',
	user               : host + '/getuser',
	users              : host + '/users',
	sitemenu           : host + '/mnuses',
	seo                : host + '/seos',
	seodel             : host + '/seodel',
	social             : host + '/rsocials',
	cpass              : host + '/changepass',
	marketings         : host + '/marketings',

	metrika            : 'http://metrics.nixteam.ru',
	siteHost           : 'https://www.beicon.ru',
}

export default cfg
