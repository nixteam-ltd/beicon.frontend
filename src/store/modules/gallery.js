import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list             : [],
	currentRow       : {},

	filters   : {
		article_id: {
			value  : undefined,
			strict : true,
		},
	},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	sorting:{
		prop  : 'id',
		order : 'descending',
		dir   : 'desc'
    },
	typesList:[],
	typesHash:{}
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.editmode = false
		key.editorsVals = {
            name: key.name,
            type: key.type,
        }
	});
	return arr
}

var postprocess = function(itm) {
	itm.editmode = false
	itm.editorsVals = {
        name: itm.name,
        type: itm.type,
    }
	return itm
}

const actions = {

	getList({commit, dispatch}, article_id) {

		let props = {
			url     : Conf.gallery,
			name    : 'gallery',
			process : process
		}

		if (article_id) {
			commit('set', {type: 'filters', items:{
				article_id: {
					value  : article_id,
					strict : true,
				}
			}});
		}
		else {
			commit('set', {type: 'filters', items:{
				article_id: {
					value  : undefined,
					strict : true,
				}
			}});
		}

		dispatch('app/get_list', props, { root: true })
	},

	getItem({commit, dispatch}, id) {

		let props = {
			url         : Conf.gallery,
			name        : 'gallery',
			id          : id,
			postprocess : postprocess
		}

		dispatch('app/get_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.gallery,
			name        : 'gallery',
			item        : item,
			postprocess : postprocess,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.gallery,
			name        : 'gallery',
			item        : item,
			postprocess : postprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.gallery,
			name : 'gallery',
			id   : id,
		}

		console.log(props)

		dispatch('app/delete_item', props, { root: true })
	},

	types({commit}) {
		let array = [
			{id: 'default', name: 'по умолчанию'},
			{id: 'two_column', name: 'в две колонки'},
			{id: 'three_column', name: 'в три колонки'},
		]

		let h = {}
		array.forEach(key => {
			h[key.id] = key.name
		});

		commit('set', {type: 'typesList', items:array})
		commit('set', {type: 'typesHash', items:h})
	},

}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
