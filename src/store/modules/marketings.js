import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
    fullList :[],
    fullHash :{}
}

const getters = {}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.marketings,
			name  : 'marketings',
		}

		dispatch('app/get_list', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.marketings,
			name : 'marketings',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.marketings,
			name        : 'marketings',
			item        : item,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.marketings,
			name        : 'marketings',
			item        : item,
		}

		dispatch('app/update_item', props, { root: true })
	},

	getAll({commit, dispatch}) {

		let props = {
			url  : Conf.marketings,
			name : 'marketings',
			list : 'fullList',
			hash : 'fullHash',
		}

		dispatch('app/get_all', props, { root: true })
	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
