import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		name   : {
			value  : undefined,
			strict : false,
		},
		url    : {
			value  : undefined,
			strict : false,
		},
		sort   : {
			value  : undefined,
			strict : true,
		},
		hidden : {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'sort',
		order : 'ascending',
		dir   : 'asc'
    },
    fullList :[],
    fullHash :{},
    categoryList: []
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.hidden = !!+key.hidden
	});
	return arr
}

var postprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	return itm
}

var preprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.sections.url,
			name  : 'sections',
			process: process
		}

		dispatch('app/get_list', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.sections.url,
			name : 'sections',
			id   : id,
			seo  : {
				tbl : Conf.sections.tbl,
			}
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.sections.url,
			name        : 'sections',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
			seo         : {
				tbl : Conf.sections.tbl,
			}
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.sections.url,
			name        : 'sections',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	getAll({commit, dispatch}) {

		let props = {
			url  : Conf.sections.url,
			name : 'sections',
			list : 'fullList',
			hash : 'fullHash',
		}

		dispatch('app/get_all', props, { root: true })
	},

	getCategories({dispatch, commit, rootState}, id) {
		dispatch('app/clear_errors', {loader: true}, { root: true })
		commit('set', {type: 'categoryList', items:[]})

		let arg = {
			'access-token' : Cookies.get('tkn_'),
		}

		Vue.http.get(Conf.getSectCategories + '/' + id, {params: arg}).then(
			response => {
				let body = response.body
				// console.log(body)

				if (body) {
					body.forEach(key => {
						key.value = !!+key.value
					});
					commit('set', {type: 'categoryList', items:body})
				}
				else {
					commit('set', {type: 'categoryList', items:[]})
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })
			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	saveCategories({dispatch, commit, rootState}, item) {
		dispatch('app/clear_errors', {loader: true}, { root: true })

		let data = {}
		data.value = (item.value) ? 1 : 0
		data.section_id = item.id

		Vue.http.post(Conf.saveSectCategories + '/' + state.currentRow.id + '?access-token=' + Cookies.get('tkn_') , data).then(
			response => {
				let body = response.body
				if (body && body.status && body.status === 'success') {
					commit('app/set', {type: 'editResult', items:'success'}, { root: true });
				}
				else {
					commit('app/set', {type: 'editResult', items:'error'}, { root: true });
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })

			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	getSEO({dispatch, commit, rootState}, item) {

		let props = {
			name : 'sections',
			tbl  : Conf.sections.tbl,
		}

		dispatch('app/get_seo', props, { root: true })
	}
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
