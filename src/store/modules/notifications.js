import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list       : [],

	filters   : {},
}

const getters = {}

const actions = {
	getList({commit, dispatch, rootState}) {

		if (!rootState.auth.user.id)
			return

		dispatch('app/clear_errors', {loader: false}, { root: true })

		let arg = {
			'access-token' : Cookies.get('tkn_'),
			sorting   : 'date_create',
			sortingby : 'desc',
			psize     : 0,
			user_whom : rootState.auth.user.id
		}

		return Vue.http.get(Conf.notifications, {params: arg}).then(
			response => {
				let body = response.body
				if ('data' in body) {
					commit('set', {type: 'list', items:body.data})
				}
			},
			error => {
				dispatch('app/process_errors', error, { root: true })
            }
		)
	},

	deleteItem({dispatch, commit, rootState}, id) {

		let props = {
			url  : Conf.notifications,
			name : 'notifications',
			id   : id,
		}

		dispatch('app/delete_item', props, { root: true })

		// commit('app/set', {type: 'loader', items:true}, { root: true })

		// let array = state.list
		// let index = array.findIndex(x => x.id === id)
		// array.splice(index, 1)
		// commit('set', {type: 'list', items: array})

		// commit('app/set', {type: 'delResult', items:'success'}, { root: true });
		// setTimeout(() => {
  //         commit('app/set', {type: 'loader', items:false}, { root: true })
  //       }, 300);
	},

	addItem({dispatch, commit, rootState}, data) {

		let record = {
			user_from  : rootState.auth.user.id,
			article_id : data.article.id,
			type       : (data.type === 'update') ? 'info' : 'primary'
		}

		if (rootState.auth.user.role === 'admin') {
			record.user_whom = data.article.author
			if (record.user_from != record.user_whom)
				Vue.http.post(Conf.notifications + '?access-token=' + Cookies.get('tkn_'), record)
		}


	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
