import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

import { routerMap } from '../../router/router'

Vue.use(VueResource)

const state = {
	sidebar: {
  	opened: !+Cookies.get('sidebarStatus'),
  	withoutAnimation: false
  },

  routes         : [],

  addResult      : '',
  editResult     : '',
  delResult      : '',
  validResult    : '',
  loader         : false,
  noResponse     : false,
  error_404      : false,
  showChangePass : false,
}

const getters = {}

const actions = {

    clear_errors({commit}, params) {
        commit('set', {type: 'noResponse', items:false})
        commit('set', {type: 'error_404', items:false})
        if (params && 'loader' in params && params.loader)
            commit('set', {type: 'loader', items:true})
    },

    process_errors({commit}, params) {
        console.log(params)
        if (params.loader)
            commit('set', {type: 'loader', items:false})

        if (!params.status) {
            commit('set', {type: 'noResponse', items:true})
        }

        else if (params.status == 404 && params.err404) {
            commit('set', {type: 'error_404', items:true})
        }

        else if (params.status == 422) {
            let mes = [params.statusText]
            console.log(params.body)
            params.body.forEach(key => {
                mes.push(key.message)
            });
            commit('set', {type: 'validResult', items:mes.join(" ")})
        }

        else if (params.status == 401) {
            commit('auth/set', {type: 'isAuthed', items:false}, { root: true })
        }

    },

    get_list({commit, dispatch, rootState}, props){

        let loaderOn = true
        if ('noloader' in props && props.noloader)
            loaderOn = false

        dispatch('clear_errors', {loader: loaderOn})

        commit(props.name + '/set', {type: 'list', items:[]}, { root: true })

        let filtered = false
        let arg = {
            'access-token': Cookies.get('tkn_')
        }

        let st = rootState[props.name]

        if (st.filters) {
            Object.keys(st.filters).map((filter_key) => {
                let filter = st.filters[filter_key].value

                if (!filter || filter === null) {
                    filter = ''
                } else {
                    filtered = true
                }


                if (filter) {
                    let strict = st.filters[filter_key].strict

                    if (filter === 'false')
                        arg[filter_key] = 0
                    else if (filter === 'true')
                        arg[filter_key] = 1
                    else if (typeof filter === 'number' || strict)
                        arg[filter_key] = filter
                    else
                        arg[filter_key] = "%" + filter
                }
            })
        }

        if (st.perPage) arg.psize = st.perPage

        arg.page = st.currentPage
        // if (!filtered) arg.page = st.currentPage

        if (st.sorting && st.sorting.prop) {
            arg.sorting   = st.sorting.prop
            arg.sortingby = st.sorting.dir
        }

        return Vue.http.get(props.url, {params: arg}).then(
            response => {
                if (loaderOn) commit('set', {type: 'loader', items:false})
                let body = response.body
                // console.log(body)

                if ('data' in body) {

                    let list = ('process' in props) ? props.process(body.data) : body.data
                    commit(props.name + '/set', {type: 'list', items:list}, { root: true })

                    if (body._meta) {
                        commit(props.name + '/set', {type: 'currentPage', items:body._meta.currentPage}, { root: true })
                        commit(props.name + '/set', {type: 'totalCount', items:body._meta.totalCount}, { root: true })
                        commit(props.name + '/set', {type: 'perPage', items:body._meta.perPage}, { root: true })
                    }
                }
            },
            error => {
                if (loaderOn) error.loader = true
                dispatch('process_errors', error)
            }
        )
    },

    get_item({commit, dispatch, rootState}, props){
        dispatch('clear_errors', {loader: true})

        commit(props.name + '/set', {type: 'currentRow', items:{}}, { root: true })

        let arg = {
            params: {
                'access-token': Cookies.get('tkn_')
            },
        }

        return Vue.http.get(props.url + '/' + props.id, arg).then(
            response => {
                let body = response.body
                // console.log(body)

                if (body) {
                    commit('set', {type: 'loader', items:false})
                    let item = ('data' in body) ? body.data : body
                    item = ('postprocess' in props) ? props.postprocess(item) : item
                    // console.log(item)
                    commit(props.name + '/set', {type: 'currentRow', items:item}, { root: true })
                }
            },
            error => {
                error.loader = true
                error.err404 = true
                dispatch('process_errors', error)
            }
        )
    },

    get_all({commit, dispatch, rootState}, props){
        dispatch('clear_errors', {loader: false})

        commit(props.name + '/set', {type: props.list, items:[]}, { root: true })
        commit(props.name + '/set', {type: props.hash, items:{}}, { root: true })

        let arg = {
            params: {
                psize : 0,
                'access-token': Cookies.get('tkn_')
            },
            headers: {
                'Content-Type': 'application/json'
            }
        }

        return Vue.http.get(props.url, arg).then(
            response => {
                let body = response.body
                // console.log(body)

                if ('data' in body) {

                    let h = {}

                    body.data.forEach(key => {
                        h[key.id] = key.name
                    });

                    commit(props.name + '/set', {type: props.list, items:body.data}, { root: true })
                    commit(props.name + '/set', {type: props.hash, items:h}, { root: true })
                }
            },
            error => {
                error.loader = false
                dispatch('process_errors', error)
            }
        )
    },

    delete_item({commit, dispatch, rootState}, props){
        dispatch('clear_errors', {loader: true})

        let st = rootState[props.name]

        return Vue.http.delete(props.url + '/' + props.id + '?access-token=' + Cookies.get('tkn_')).then(
            response => {
                let body = response.body
                commit('set', {type: 'delResult', items:'success'})
                commit('set', {type: 'loader', items:false})

                let array = st.list
                let index = array.findIndex(x => x.id === props.id)
                array.splice(index, 1)
                commit(props.name + '/set', {type: 'list', items:array}, { root: true })

                if ('seo' in props) {
                    dispatch('delete_seo', {
                        tbl       : props.seo.tbl,
                        id_record : props.id,
                    })
                }

            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'delResult', items:'error'});
            }
        )
    },

    add_item({commit, dispatch, rootState}, props){
        dispatch('clear_errors', {loader: true})

        let st = rootState[props.name]

        let data = ('preprocess' in props) ? props.preprocess(props.item) : props.item

        return Vue.http.post(props.url + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body
                // console.log(response)
                // console.log(body)

                commit('set', {type: 'addResult', items:'success'})
                commit('set', {type: 'loader', items:false})

                let array = st.list
                let item = ('data' in body) ? body.data : body
                item = ('postprocess' in props) ? props.postprocess(item) : item
                array.unshift(item)

                commit(props.name + '/set', {type: 'list', items:array}, { root: true })
                commit(props.name + '/set', {type: 'currentRow', items:item}, { root: true })

                if ('seo' in props) {
                    dispatch('add_seo', {
                        tbl        : props.seo.tbl,
                        id_record  : item.id,
                        title      : item.name,
                        og_title   : item.name,
                        og_locale  : 'ru_RU',
                        changegreq : 'always',
                    })
                }
            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'addResult', items:'error'});
            }
        )
    },

    update_item({commit, dispatch, rootState}, props){
        dispatch('clear_errors', {loader: true})

        let st = rootState[props.name]

        let data = ('preprocess' in props) ? props.preprocess(props.item) : props.item

        return Vue.http.put(props.url + '/' + data.id + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body
                if (body){
                    commit('set', {type: 'editResult', items:'success'})
                    commit('set', {type: 'loader', items:false})

                    let array = st.list
                    let item = ('data' in body) ? body.data : body
                    // console.log(item)
                    item = ('postprocess' in props) ? props.postprocess(item) : item
                    let index = array.findIndex(x => x.id === item.id)
                    Vue.set(array, index, item)

                    commit(props.name + '/set', {type: 'list', items:array}, { root: true })
                    commit(props.name + '/set', {type: 'currentRow', items:item}, { root: true })
                }
            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'editResult', items:'error'});
            }
        )
    },

    get_seo({commit, dispatch, rootState}, props){

        commit(props.name + '/set', {type: 'seo', items:{}}, { root: true })

        dispatch('clear_errors', {loader: true})

        let st = rootState[props.name]
        let record_id = st.currentRow.id
        if (!record_id) {
            commit('set', {type: 'loader', items:false})
            return
        }

        let arg = {
            'access-token': Cookies.get('tkn_'),
            tbl           : props.tbl,
            id_record     : record_id,
        }

        return Vue.http.get(Conf.seo, {params: arg}).then(
            response => {
                commit('set', {type: 'loader', items:false})
                let seo = {}
                let body = response.body
                // console.log(body)

                if ('data' in body) {
                    let list = body.data
                    if (list[0]) { seo = list[0] }
                    else {
                        seo.tbl        = props.tbl
                        seo.id_record  = record_id
                        seo.title      = st.currentRow.name
                        seo.og_title   = st.currentRow.name
                        seo.og_locale  = 'ru_RU'
                        seo.changegreq = 'always'
                    }

                    commit(props.name + '/set', {type: 'seo', items:seo}, { root: true })
                }
            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
            }
        )
    },

    add_seo({commit, dispatch, rootState}, data){
        dispatch('clear_errors', {loader: true})

        return Vue.http.post(Conf.seo + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body
                // console.log(response)
                // console.log(body)

                commit('set', {type: 'addResult', items:'success'})
                commit('set', {type: 'loader', items:false})

            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'addResult', items:'error'});
            }
        )
    },

    edit_seo({commit, dispatch, rootState}, data){
        dispatch('clear_errors', {loader: true})

        return Vue.http.put(Conf.seo + '/' + data.id + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body
                // console.log(response)
                // console.log(body)

                commit('set', {type: 'editResult', items:'success'})
                commit('set', {type: 'loader', items:false})

            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'editResult', items:'error'});
            }
        )
    },

    delete_seo({commit, dispatch, rootState}, data){
        dispatch('clear_errors', {loader: true})

        return Vue.http.post(Conf.seodel + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body

                commit('set', {type: 'delResult', items:'success'})
                commit('set', {type: 'loader', items:false})

            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'delResult', items:'error'});
            }
        )
    },

    closeSideBar({commit}) {
        Cookies.set('sidebarStatus', 1)
        commit('set', {type: 'sidebar', items: {opened: false, withoutAnimation: false}});
    },

    toggleSideBar({commit}) {
        if (state.sidebar.opened) {
            Cookies.set('sidebarStatus', 1)
        } else {
            Cookies.set('sidebarStatus', 0)
        }
        state.sidebar.opened = !state.sidebar.opened
        state.sidebar.withoutAnimation = false
    },

    generateRoutes({commit, rootState, dispatch}) {
        var r_hash = {}
        var role = rootState.auth.user.role

        commit('set', {type: 'routes', items: [] });

        routerMap.forEach(route => {

            var rt = JSON.parse(JSON.stringify(route))

            var canShow = false

            if (!rt.hidden) {
                if (rt.meta && rt.meta.roles)
                    canShow = (rt.meta.roles.indexOf(role) >= 0) ? true : false
                else
                    canShow = true
            }

            if (canShow){
                if (rt.meta && rt.meta.parent) {
                    if (r_hash[rt.meta.parent].children && r_hash[rt.meta.parent].children.length) {
                        r_hash[rt.meta.parent].children.push(rt);
                    }
                    else {
                        r_hash[rt.meta.parent].children = [rt];
                    }
                }
                else {
                    r_hash[rt.name] = rt
                }
            }
        })

        commit('set', {type: 'routes', items: Object.values(r_hash) });
    },

    changePass({commit, dispatch}, data) {
        dispatch('clear_errors', {loader: true})

        let arg = {
            password     : data.old_password,
            new_password : data.new_password,
        }

        return Vue.http.post(Conf.cpass + '?access-token=' + Cookies.get('tkn_'), arg).then(
          response => {
                let body = response.body
                // console.log(response)
                if (body && !'error' in body) {
                    commit('set', {type: 'editResult', items:'success'})
                    commit('set', {type: 'loader', items:false})
                    commit('set', {type: 'showChangePass', items:false})
                }
                else {
                    commit('set', {type: 'editResult', items:'error'})
                    commit('set', {type: 'loader', items:false})
                }
            },
            error => {
                error.loader = true
                dispatch('process_errors', error)
                commit('set', {type: 'editResult', items:'error'});
            }
        )
    }
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
