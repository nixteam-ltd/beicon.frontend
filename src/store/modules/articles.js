import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list            : [],
	currentRow      : {},
	seo             : {},
	totalCount      : 0,
	perPage         : 10,
	currentPage     : 1,
	filters   : {
		name         : {
			value  : undefined,
			strict : false,
		},
		url          : {
			value  : undefined,
			strict : false,
		},
		choise       : {
			value  : undefined,
			strict : false,
		},
		not_miss     : {
			value  : undefined,
			strict : false,
		},
		topic_day    : {
			value  : undefined,
			strict : false,
		},
		section      : {
			value  : undefined,
			strict : true,
		},
		author       : {
			value  : undefined,
			strict : true,
		},
		date_publish : {
			value  : undefined,
			strict : true,
		},
		date_create  : {
			value  : undefined,
			strict : true,
		},
		status       : {
			value  : undefined,
			strict : true,
		},
		main_sort    : {
			value  : undefined,
			strict : true,
		},
		show_on_main : {
			value  : undefined,
			strict : false,
		},
		section_topic : {
			value  : undefined,
			strict : false,
		},
		views : {
			value  : undefined,
			strict : false,
		},
	},
	sorting         :{
		prop            : 'date_create',
		order           : 'descending',
		dir             : 'desc'
	},
	statusList      :[],
	statusHash      :{},
	viewList        :[],
	viewHash        :{},
	rssList         :[],
	galeryList      :[],
	leadersList     :[],
	recommendedList :[],
	recommendedData: {
		currentPage : 1,
		totalCount  : 0,
		perPage     : 20,
		sorting:{
			prop  : 'value',
			order : 'descending',
			dir   : 'desc'
	    },
	}
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.choise        = !!+key.choise
		key.not_miss      = !!+key.not_miss
		key.topic_day     = !!+key.topic_day
		key.show_on_main  = !!+key.show_on_main
		key.section_topic = !!+key.section_topic

		if (key.preview_img) {
			key.preview_img.forEach(img => {
				if (img.postfix && img.postfix === 'admin'){
					key.preview_img_admin = img.url
				}
				else if (!img.postfix) {
					key.preview_img_original = img.image
				}
				else if (img.postfix === '1x1') {
					img.dims = [1,1]
				}
				else if (img.postfix === '16x9') {
					img.dims = [16,9]
				}
				else if (img.postfix === '9x16') {
					img.dims = [9,16]
				}
				else if (img.postfix === '19x6') {
					img.postfix === '9x16'
					img.dims = [9,16]
				}
			})
		}

		if (key.header_img) {
			key.header_img.forEach(img => {
				if (img.postfix && img.postfix === 'admin') {
					key.header_img_admin = img.url
				}
				else if (!img.postfix) {
					key.header_img_original = img.image
				}
				else if (img.postfix === '1x1') {
					img.dims = [1,1]
				}
				else if (img.postfix === '16x9') {
					img.dims = [16,9]
				}
				else if (img.postfix === '9x16') {
					img.dims = [9,16]
				}
				else if (img.postfix === '19x6') {
					img.postfix === '9x16'
					img.dims = [9,16]
				}
			})
		}
	});
	return arr
}

var postprocess = function(itm) {
	['choise', 'not_miss', 'topic_day','show_on_main', 'section_topic'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	itm.noAlert     = false
	itm.openPreview = false

	if (itm.preview_img) {
		itm.preview_img.forEach(img => {
			if (img.postfix && img.postfix === 'admin'){
				itm.preview_img_admin = img.url
			}
			else if (!img.postfix) {
				itm.preview_img_original = img.image
			}
			else if (img.postfix === '1x1') {
				img.dims = [1,1]
			}
			else if (img.postfix === '16x9') {
				img.dims = [16,9]
			}
			else if (img.postfix === '9x16') {
				img.dims = [9,16]
			}
			else if (img.postfix === '19x6') {
				img.postfix === '9x16'
				img.dims = [9,16]
			}
		})
	}

	if (itm.header_img) {
		itm.header_img.forEach(img => {
			if (img.postfix && img.postfix === 'admin') {
				itm.header_img_admin = img.url
			}
			else if (!img.postfix) {
				itm.header_img_original = img.image
			}
			else if (img.postfix === '1x1') {
				img.dims = [1,1]
			}
			else if (img.postfix === '16x9') {
				img.dims = [16,9]
			}
			else if (img.postfix === '9x16') {
				img.dims = [9,16]
			}
			else if (img.postfix === '19x6') {
				img.postfix === '9x16'
				img.dims = [9,16]
			}
		})
	}

	return itm
}

var preprocess = function(itm) {
	['choise', 'not_miss', 'topic_day','show_on_main', 'section_topic'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {
	getList({commit, dispatch, rootState}) {

		let user = rootState.auth.user

		if (user.role === 'editor')
			commit('set_filter', {type: 'author', items: user.id})

		let props = {
			url     : Conf.articles.url,
			name    : 'articles',
			process : process
		}

		dispatch('app/get_list', props, { root: true })
	},

	getItem({commit, dispatch}, id) {

		let props = {
			url         : Conf.articles.url,
			name        : 'articles',
			id          : id,
			postprocess : postprocess
		}

		dispatch('app/get_item', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.articles.url,
			name : 'articles',
			id   : id,
			seo  : {
				tbl : Conf.articles.tbl,
			}
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.articles.url,
			name        : 'articles',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
			seo  : {
				tbl : Conf.articles.tbl,
			}
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {
		dispatch('app/clear_errors', {loader: true}, { root: true })

		let noAlert     = ('noAlert' in item && item.noAlert) ? 1 : 0;
		let openPreview = ('openPreview' in item && item.openPreview) ? 1 : 0;

		let data = preprocess(item)
		// console.log(item)

		commit('app/set', {type: 'loader', items:true}, { root: true })

		return Vue.http.put(Conf.articles.url + '/' + data.id + '?access-token=' + Cookies.get('tkn_'), data).then(
          response => {
                let body = response.body
                if (body){
                    if (!noAlert)
                    	commit('app/set', {type: 'editResult', items:'success'}, { root: true });

                    commit('app/set', {type: 'loader', items:false}, { root: true })

                    let array = state.list
                    let ret_item = ('data' in body) ? body.data : body
                    ret_item = postprocess(ret_item)
                    let index = array.findIndex(x => x.id === ret_item.id)
                    Vue.set(array, index, ret_item)

                    if (ret_item.topic_day) {
                    	array.forEach(key => {
							if (key.id != ret_item.id)
								key.topic_day = false
						});
                    }

                    commit('set', {type: 'list', items:array})
                    commit('set', {type: 'currentRow', items:ret_item})

                    if (openPreview && ret_item.preview_href)
						window.open(ret_item.preview_href)

					if ('id' in ret_item) dispatch('notifications/addItem', {type: 'update', article: ret_item}, {root: true})
                }
            },
            error => {
                error.loader = true
                dispatch('app/process_errors', error, { root: true })
                commit('app/set', {type: 'editResult', items:'error'}, { root: true });
            }
        )
	},

	statuses({commit}) {
		let array = [
			{id: 'draft', name: 'черновик'},
			{id: 'in_work', name: 'в работе'},
			{id: 'redraft', name: 'на доработке'},
			{id: 'moderate', name: 'ожидает модерации'},
			{id: 'in_moderate', name: 'на модерации'},
			{id: 'publish', name: 'опубликовано'},
		]

		let h = {}
		array.forEach(key => {
			h[key.id] = key.name
		});

		commit('set', {type: 'statusList', items:array})
		commit('set', {type: 'statusHash', items:h})
	},

	views({commit}) {
		let array = [
			{id: 'article', name: 'статья'},
			{id: 'gallery-only', name: 'галерея в анонсе'},
			{id: 'gallery-one-column', name: 'галерея с одной колонкой'},
			{id: 'gallery-only-one-column', name: 'галерея в анонсе с одной колонкой'},
		]

		let h = {}
		array.forEach(key => {
			h[key.id] = key.name
		});

		commit('set', {type: 'viewList', items:array})
		commit('set', {type: 'viewHash', items:h})
	},

	getRecommended({dispatch, commit, rootState}, data) {
		dispatch('app/clear_errors', {loader: true}, { root: true })
		commit('set', {type: 'recommendedList', items:[]})

		let id     = data.article_id
		let filter = data.filter

		let arg = {
			'access-token' : Cookies.get('tkn_'),
		}

		let filtered = false
		if (filter) {
			if (filter.value && filter.value === 'true') {
				arg.value = 1
				filtered = true
			}
			if (filter.value && filter.value === 'false') {
				arg.value = 0
				filtered = true
			}
			if (filter.name) {
				arg.name = filter.name
				filtered = true
			}
			if (filter.section_id) {
				arg.section_id = filter.section_id
				filtered = true
			}
		}

		if (state.recommendedData.perPage) arg.psize = state.recommendedData.perPage

		if (state.recommendedData.sorting && state.recommendedData.sorting.prop) {
			let sorting = state.recommendedData.sorting.prop
			if (state.recommendedData.sorting.dir === 'desc')
				sorting = '-' + sorting
            arg.sort = sorting
        }

        // if (!filtered) arg.page = state.recommendedData.currentPage
        arg.page = state.recommendedData.currentPage

		Vue.http.get(Conf.recArticles + '/' + id, {params: arg}).then(
			response => {
				let body = response.body
				// console.log(body)

				if (body && 'data' in body) {
					body.data.forEach(key => {
						key.value = !!+key.value
					});
					commit('set', {type: 'recommendedList', items:body.data})

					if (body._meta) {
                        commit('set_recommended', {type: 'currentPage', items:body._meta.currentPage})
                        commit('set_recommended', {type: 'totalCount', items:body._meta.totalCount})
                        commit('set_recommended', {type: 'perPage', items:body._meta.perPage})
                    }
				}
				else {
					commit('set', {type: 'recommendedList', items:[]})
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })
			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	saveRecommended({dispatch, commit, rootState}, item) {
		dispatch('app/clear_errors', {loader: true}, { root: true })

		let data = {}
		data.value = (item.value) ? 1 : 0
		data.article_id = item.id

		Vue.http.post(Conf.recArticles + '/' + state.currentRow.id + '?access-token=' + Cookies.get('tkn_'), data).then(
			response => {
				let body = response.body
				if (body && body.status && body.status === 'success') {
					commit('app/set', {type: 'editResult', items:'success'}, { root: true });
				}
				else {
					commit('app/set', {type: 'editResult', items:'error'}, { root: true });
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })

			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	getRss({dispatch, commit, rootState}, id) {
		dispatch('app/clear_errors', {loader: true}, { root: true })
		commit('set', {type: 'rssList', items:[]})

		let arg = {
			'access-token' : Cookies.get('tkn_'),
		}

		Vue.http.get(Conf.rssArticles + '/' + id, {params: arg}).then(
			response => {
				let body = response.body

				if (body) {
					body.forEach(key => {
						key.value = !!+key.value
					});
					commit('set', {type: 'rssList', items:body})
				}
				else {
					commit('set', {type: 'rssList', items:[]})
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })
			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	saveRss({dispatch, commit, rootState}, item) {
		dispatch('app/clear_errors', {loader: true}, { root: true })

		let data = {}
		data.value = (item.value) ? 1 : 0
		data.rss_id = item.id

		Vue.http.post(Conf.rssArticles + '/' + state.currentRow.id + '?access-token=' + Cookies.get('tkn_'), data).then(
			response => {
				let body = response.body
				if (body && body.status && body.status === 'success') {
					commit('app/set', {type: 'editResult', items:'success'}, { root: true });
				}
				else {
					commit('app/set', {type: 'editResult', items:'error'}, { root: true });
				}
				commit('app/set', {type: 'loader', items:false}, { root: true })

			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
			}
		)
	},

	getLeadersList({commit, dispatch, rootState}) {
		dispatch('app/clear_errors', {loader: true}, { root: true })
		commit('set', {type: 'leadersList', items:[]})

		let arg = {
			'access-token' : Cookies.get('tkn_'),
			page      : 1,
			sorting   : 'views',
			sortingby : 'desc',
			psize     : 30
		}

		return Vue.http.get(Conf.articles.url, {params: arg}).then(
            response => {

                let body = response.body

                if ('data' in body) {

					let result = []
					let list   = body.data

                    list.forEach(key => {
						result.push([key.name, key.views])
					});

					commit('set', {type: 'leadersList', items:result})
                }

                commit('app/set', {type: 'loader', items:false}, { root: true })
            },
            error => {
                error.loader = true
				dispatch('app/process_errors', error, { root: true })
            }
        )

	},

	getSEO({dispatch, commit, rootState}, item) {

		let props = {
			name : 'articles',
			tbl  : Conf.articles.tbl,
		}

		dispatch('app/get_seo', props, { root: true })
	}
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	},
	set_filter(state, {type, items}) {
		state.filters[type].value = items
	},
	set_recommended(state, {type, items}) {
		state.recommendedData[type] = items
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
