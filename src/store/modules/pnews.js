import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		name   : {
			value  : undefined,
			strict : false,
		},
		url    : {
			value  : undefined,
			strict : false,
		},
		sort   : {
			value  : undefined,
			strict : true,
		},
		hidden : {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'sort',
		order : 'ascending',
		dir   : 'asc'
    },
    fullList :[],
    fullHash :{}
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.hidden = !!+key.hidden

		if (key.preview_img) {
			key.preview_img.forEach(img => {
				if (img.postfix && img.postfix === 'admin')
					key.preview_img_admin = img.image
				if (!img.postfix)
					key.preview_img_original = img.image
			})
		}
	});

	return arr
}

var postprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
	});

	if (itm.preview_img) {
		itm.preview_img.forEach(img => {
			if (img.postfix && img.postfix === 'admin')
				itm.preview_img_admin = img.image
			if (!img.postfix)
				itm.preview_img_original = img.image
		})
	}

	return itm
}

var preprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.pnews,
			name  : 'pnews',
			process: process
		}

		dispatch('app/get_list', props, { root: true })
	},

	getItem({commit, dispatch}, id) {

		let props = {
			url         : Conf.pnews,
			name        : 'pnews',
			id          : id,
			postprocess : postprocess
		}

		dispatch('app/get_item', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {

		let props = {
			url  : Conf.pnews,
			name : 'pnews',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.pnews,
			name        : 'pnews',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.pnews,
			name        : 'pnews',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	updateItemInList({dispatch, commit, rootState}) {

		let array = state.list
		let index = array.findIndex(x => x.id === state.currentRow.id)
		Vue.set(array, index, state.currentRow)

		commit('set', {type: 'list', items:array})
	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
