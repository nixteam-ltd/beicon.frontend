import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list          : [],
	currentRow    : {},
	variationsVis : false,

	filters   : {
		gallery_id : {
			value  : undefined,
			strict : true,
		},
		psize      : {
			value  : undefined,
			strict : true,
		}
	},

	sorting:{
		prop  : 'sort',
		order : 'ascending',
		dir   : 'asc'
    },
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.editmode = false
		key.editorsVals = {
			content : key.content,
			sort    : key.sort,
        }

        if (key.sizes)
	        key.sizes.forEach(size => {
				if (size.postfix === '1x1') {
					key.preview = size.url
					size.dims = [1,1]
				}
				else if (size.postfix === '16x9') {
					size.dims = [16,9]
				}
				else if (size.postfix === '9x16') {
					size.dims = [9,16]
				}
				else if (size.postfix === '19x6') {
					size.postfix === '9x16'
					size.dims = [9,16]
				}
			})
	});
	return arr
}

var postprocess = function(itm) {
	itm.editmode = false
	itm.editorsVals = {
		content : itm.content,
		sort    : itm.sort,
    }
    if (itm.sizes)
	 	itm.sizes.forEach(size => {
			if (size.postfix === '1x1') {
				itm.preview = size.url
				size.dims = [1,1]
			}
			else if (size.postfix === '16x9') {
				size.dims = [16,9]
			}
			else if (size.postfix === '9x16') {
				size.dims = [9,16]
			}
			else if (size.postfix === '19x6') {
				size.postfix === '9x16'
				size.dims = [9,16]
			}
		})
	return itm
}

const actions = {

	getList({commit, dispatch}, gallery_id) {
		if (!gallery_id) return

		let props = {
			url     : Conf.galleryitems,
			name    : 'galleryitems',
			process : process
		}

		commit('set', {type: 'filters', items:{
			gallery_id: {
				value  : gallery_id,
				strict : true,
			},
			psize: {
				value  : 'false',
				strict : true,
			}}});

		dispatch('app/get_list', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.galleryitems,
			name        : 'galleryitems',
			item        : item,
			postprocess : postprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.galleryitems,
			name : 'galleryitems',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	changeImageSort({dispatch, commit, rootState}, data) {

		dispatch('app/clear_errors', {loader: true}, { root: true })

		let images     = data.list
		let gallery_id = data.gallery_id

		let array = []
		var newsort = 0
		images.forEach(key => {
			newsort = newsort + 10
			array.push({id: key.id, sort: newsort})
		})

		Vue.http.post(Conf.gallerysort + '?access-token=' + Cookies.get('tkn_'), array).then(
			response => {
				commit('app/set', {type: 'editResult', items:'success'}, { root: true });
				commit('app/set', {type: 'loader', items:false}, { root: true });

				dispatch('getList', gallery_id)
			},
			error => {
				error.loader = true
				dispatch('app/process_errors', error, { root: true })
				commit('app/set', {type: 'editResult', items:'error'}, { root: true });
			}
		)
	}
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
