import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,

	filters: {
		name            : {
			value  : undefined,
			strict : false,
		},
		username        : {
			value  : undefined,
			strict : false,
		},
		role            : {
			value  : undefined,
			strict : true,
		},
		status          : {
			value  : undefined,
			strict : true,
		},
		last_login_date : {
			value  : undefined,
			strict : true,
		},
		email           : {
			value  : undefined,
			strict : false,
		},
    },
    sorting:{
		prop  : 'name',
		order : 'ascending',
		dir   : 'asc'
    },

	fullList :[],
    fullHash :{},

    rolesList:[],
    rolesHash:{},
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.status = !!+key.status
	});
	return arr
}

var postprocess = function(itm) {
	['status'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	return itm
}

var preprocess = function(itm) {
	['status'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url     : Conf.users,
			name    : 'users',
			process : process
		}

		return dispatch('app/get_list', props, { root: true })

	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.users,
			name : 'users',
			id   : id
		}

		return dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.users,
			name        : 'users',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		return dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.users,
			name        : 'users',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		return dispatch('app/update_item', props, { root: true })
	},

	getAll({commit, dispatch}) {
		let props = {
			url  : Conf.users,
			name : 'users',
			list : 'fullList',
			hash : 'fullHash',
		}

		return dispatch('app/get_all', props, { root: true })
	},

	roles({commit}) {
		let array = [
			{id: 'admin', name: 'администратор'},
			{id: 'editor', name: 'редактор'},
			{id: 'seo', name: 'seo-специалист'},
			{id: 'programmer', name: 'программист'},
		]

		let h = {}
		array.forEach(key => {
			h[key.id] = key.name
		});

		commit('set', {type: 'rolesList', items:array})
		commit('set', {type: 'rolesHash', items:h})
	},

}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
