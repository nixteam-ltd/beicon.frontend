import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		name   : {
			value  : undefined,
			strict : false,
		},
		url    : {
			value  : undefined,
			strict : false,
		},
		sort   : {
			value  : undefined,
			strict : true,
		},
		hidden : {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'sort',
		order : 'ascending',
		dir   : 'asc'
    },
    fullList :[],
    fullHash :{}
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.hidden = !!+key.hidden
	});
	return arr
}

var postprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	return itm
}

var preprocess = function(itm) {
	['hidden'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.sitemenu,
			name  : 'sitemenu',
			process: process
		}

		dispatch('app/get_list', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.sitemenu,
			name : 'sitemenu',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.sitemenu,
			name        : 'sitemenu',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.sitemenu,
			name        : 'sitemenu',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	getAll({commit, dispatch}) {

		let props = {
			url  : Conf.sitemenu,
			name : 'sitemenu',
			list : 'fullList',
			hash : 'fullHash',
		}

		dispatch('app/get_all', props, { root: true })
	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
