import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		email   : {
			value  : undefined,
			strict : false,
		},
		active : {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'email',
		order : 'ascending',
		dir   : 'asc'
    },
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.active = !!+key.active
	});
	return arr
}

var postprocess = function(itm) {
	['active'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	return itm
}

var preprocess = function(itm) {
	['active'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.subscribers,
			name  : 'subscribers',
			process: process
		}

		dispatch('app/get_list', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.subscribers,
			name : 'subscribers',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.subscribers,
			name        : 'subscribers',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.subscribers,
			name        : 'subscribers',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
