import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'
import moment      from 'moment'

Vue.use(VueResource)

const appdata = {
	client_id     : 'f9d04e6ad1344104847f77d68f28f390',
	client_secret : 'd0bf285c6ce6452c817fb54eab81a4dd',
};

const state = {
	token      : '',
	counter    : '',
	badCode    : false,
	visitsList : [],
	appdata    : appdata,
}

const getters = {}


const actions = {

	getTokenByCode({commit, dispatch, rootState}, code){

		let arg = {
			params: {
				code          : code,
				client_id     : appdata.client_id,
				client_secret : appdata.client_secret,
			},
			headers: {
				'Content-Type': 'text/plain'
			}
		}

		Vue.http.post(Conf.metrika + '/get_token', null, arg).then(
			response => {
				if ('access_token' in response.body) {
					let resp_token = response.body.access_token
					commit('set', {type: 'token', items:response.body.access_token})

					dispatch('app/clear_errors', {loader: true}, { root: true })
					let arg = {
			            'access-token': Cookies.get('tkn_'),
			            keyname       : 'ya_token'
			        }
			        Vue.http.get(Conf.service, {params: arg}).then(
			        	response => {
			        		let data = response.body.data
			        		let token_record = data[0]
			        		token_record.value = resp_token
			        		commit('app/set', {type: 'loader', items:false}, { root: true })
			        		dispatch('service/updateItem', token_record, { root: true })
			        	},
			        	error => {
			                error.loader = true
			                dispatch('app/process_errors', error, { root: true })
			            }
			        )
				}

			},
			error => {
				console.log(error)
				commit('set', {type: 'badCode', items:true})
			}
		)
	},

	getVisits({commit, dispatch, rootState}){

		commit('set', {type: 'visitsList', items:[]})

		if (!state.token)
			return
		if (!state.counter)
			return

		let arg = {
			params: {
				oauth_token : state.token,
				id          : state.counter,
				group       : 'day',
				date2       : 'today',
				date1       : '30daysAgo',
				metrics     : 'ym:s:visits',
				report_path : '/stat/v1/data/bytime'
			},
			headers: {
				'Content-Type': 'text/plain'
			}
		}

		Vue.http.post(Conf.metrika + '/get_report', null, arg).then(
			response => {
				let dates = response.body.time_intervals;
				let visits = response.body.data[0].metrics[0];

				let result = [];

				dates.forEach(function (dateArray, index) {
					let date = moment(dateArray[0]).format('DD.MM')
					let val = visits[index]
					result.push([date, val])
				});

				commit('set', {type: 'visitsList', items:result})
			},
			error => {
				console.log(error)
			}
		)

	}

}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
