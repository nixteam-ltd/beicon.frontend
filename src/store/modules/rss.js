import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		name   : {
			value  : undefined,
			strict : false,
		},
		url    : {
			value  : undefined,
			strict : false,
		},
		active : {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'name',
		order : 'ascending',
		dir   : 'asc'
    },
    rssFeed : {}
}

const getters = {}

var process = function(arr) {
	arr.forEach(key => {
		key.active = !!+key.active
	});
	return arr
}

var postprocess = function(itm) {
	['active'].forEach(key => {
		itm[key] = !!+itm[key]
	});
	return itm
}

var preprocess = function(itm) {
	['active'].forEach(key => {
		itm[key] = !!+itm[key]
		itm[key] = (itm[key]) ? 1 : 0
	});
	return itm
}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.rss,
			name  : 'rss',
			process: process
		}

		dispatch('app/get_list', props, { root: true })
	},

	deleteItem({dispatch, commit, rootState}, id) {
		let props = {
			url  : Conf.rss,
			name : 'rss',
			id   : id
		}

		dispatch('app/delete_item', props, { root: true })
	},

	addItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.rss,
			name        : 'rss',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/add_item', props, { root: true })
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.rss,
			name        : 'rss',
			item        : item,
			postprocess : postprocess,
			preprocess  : preprocess,
		}

		dispatch('app/update_item', props, { root: true })
	},

	getRss({commit}){
		var parseString = require('xml2js').parseString;
		commit('set', {type: 'rssFeed', items:{}})

		return Vue.http.get(state.currentRow.path).then(
			response => {
				parseString(response.body, function (err, result) {
					if (err) {
						console.log(err)
						return false
					}
					let feed = result.rss.channel[0];

					['title', 'link'].forEach(key => {
						feed[key] = feed[key][0]
					});

					feed.item.forEach(item => {
						['author', 'link', 'pubDate', 'title', 'content'].forEach(key => {
							if (item[key]) item[key] = item[key][0]
						});

						if ('enclosure' in item) {
							let encs = []
							item.enclosure.forEach(enc => {
								let img = enc.$.url
								encs.push(img)
							});
							item.enclosure = encs
						}
					})

					// console.log(feed)

					commit('set', {type: 'rssFeed', items: feed});
				})
			},
			error => {
				console.log(error)
			},
		)
	}

}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
