import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	list        : [],
	currentRow  : {},
	seo         : {},
	currentPage : 1,
	totalCount  : 0,
	perPage     : 20,
	filters: {
		tbl : {
			value  : undefined,
			strict : false,
		},
		last_updated: {
			value  : undefined,
			strict : true,
		},
    },
    sorting:{
		prop  : 'last_updated',
		order : 'descending',
		dir   : 'desc'
    },
    fullList :[],
    fullHash :{}
}

const getters = {}

const actions = {

	getList({commit, dispatch}) {

		let props = {
			url   : Conf.seo,
			name  : 'seo',
		}

		dispatch('app/get_list', props, { root: true })
	},

	getExportData({commit, dispatch}) {

		let props = {
			url  : Conf.seo,
			name : 'seo',
			list : 'fullList',
			hash : 'fullHash',
		}

		return dispatch('app/get_all', props, { root: true }).then(() => {
			return state.fullList
		})
	},

	updateItem({dispatch, commit, rootState}, item) {

		let props = {
			url         : Conf.seo,
			name        : 'seo',
			item        : item,
		}

		dispatch('app/update_item', props, { root: true })
	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
