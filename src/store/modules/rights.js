import Vue         from 'vue'

const getters = {

	persons (state, getters, rootState) {
		let rights = {
			add       : false,
			del       : false,
			edit      : false,
			edit_item : false,
			edit_seo  : false
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add       = true
			rights.del       = true
			rights.edit      = true
			rights.edit_item = true
			rights.edit_seo  = true
		}

		else if (role === 'seo') {
			rights.edit     = true
			rights.edit_seo = true
		}

		return rights
	},

	tags (state, getters, rootState) {
		let rights = {
			add       : false,
			del       : false,
			edit      : false,
			edit_item : false,
			edit_seo  : false
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add       = true
			rights.del       = true
			rights.edit      = true
			rights.edit_item = true
			rights.edit_seo  = true
		}

		else if (role === 'seo') {
			rights.edit     = true
			rights.edit_seo = true
		}

		return rights
	},

	category (state, getters, rootState) {
		let rights = {
			add       : false,
			del       : false,
			edit      : false,
			edit_item : false,
			edit_seo  : false
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add       = true
			rights.del       = true
			rights.edit      = true
			rights.edit_item = true
			rights.edit_seo  = true
		}

		else if (role === 'seo') {
			rights.edit     = true
			rights.edit_seo = true
		}

		return rights
	},

	sections(state, getters, rootState) {
		let rights = {
			add       : false,
			del       : false,
			edit      : false,
			edit_item : false,
			edit_seo  : false
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add       = true
			rights.del       = true
			rights.edit      = true
			rights.edit_item = true
			rights.edit_seo  = true
		}

		else if (role === 'seo') {
			rights.edit     = true
			rights.edit_seo = true
		}

		return rights
	},

	seo(state, getters, rootState) {
		let rights = {
			add  : false,
			del  : false,
			edit : false,
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		else if (role === 'seo') {
			rights.add  = true
			rights.edit = true
		}

		else if (role === 'programmer') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		return rights
	},

	service(state, getters, rootState) {
		let rights = {
			add  : false,
			del  : false,
			edit : false,
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		else if (role === 'seo') {
			rights.add  = true
			rights.edit = true
		}

		else if (role === 'programmer') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		return rights
	},

	marketings(state, getters, rootState) {
		let rights = {
			add  : false,
			del  : false,
			edit : false,
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		else if (role === 'programmer') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		return rights
	},

	social(state, getters, rootState) {
		let rights = {
			add  : false,
			del  : false,
			edit : false,
		}

		let role = rootState.auth.user.role
		if (role === 'admin') {
			rights.add  = true
			rights.del  = true
			rights.edit = true
		}

		return rights
	},
}

export default {
  namespaced: true,
  getters
}
