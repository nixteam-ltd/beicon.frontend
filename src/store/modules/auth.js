import Vue         from 'vue'
import VueResource from 'vue-resource'
import Cookies     from 'js-cookie'
import Conf        from '../../../config/url.js'

Vue.use(VueResource)

const state = {
	isAuthed  : false,
	isAdmin   : false,
	isEditor  : false,
	isSeo     : false,
	isProg    : false,
	user      : {},
	authError : false,
}

const getters = {}

const actions = {

	authorization({commit, dispatch}, data) {

		Vue.http.post(Conf.auth, data).then(
			response => {
				let body = response.body
				if ( 'access-token' in body) {
					Cookies.set('tkn_', body['access-token']);
					commit('set', {type: 'isAuthed', items: true});
					dispatch('getUser')
				}
				else if (body.status === 'error') {
					commit('set', {type: 'isAuthed', items: false});
					commit('set', {type: 'authError', items: body.message});
				}
			},
			error => {
				console.log(error)
			}
		)
	},

	getUser({commit, dispatch}) {

		dispatch('app/clear_errors', {loader: false}, { root: true })
		commit('set', {type: 'user', items: {}});

		let hasToken = (Cookies.get('tkn_')) ? true : false;

		if (hasToken) {
			commit('set', {type: 'isAuthed', items: true});

			let arg = {
	            params: {
	                'access-token': Cookies.get('tkn_')
	            },
	        }

	        return Vue.http.get(Conf.user, arg).then(
	        	response => {
	        		let user = response.body.user
	        		if (user) {
	        			commit('set', {type: 'user', items: user});

		        		if (user.role === 'admin')
							commit('set', {type: 'isAdmin', items: true})
						else if (user.role === 'editor')
							commit('set', {type: 'isEditor', items: true})
						else if (user.role === 'seo')
							commit('set', {type: 'isSeo', items: true})
						else if (user.role === 'programmer')
							commit('set', {type: 'isProg', items: true})

						dispatch('app/generateRoutes', {}, { root: true })
	        		}
	        		else {
	        			commit('set', {type: 'isAuthed', items: false});
	        		}
	        	},
	        	error => {
	        		dispatch('app/process_errors', error, { root: true })
	        	}
	        )
		}
		else {
			commit('set', {type: 'isAuthed', items: false});
		}
	},

	logout({commit}) {
		Cookies.remove('tkn_');
		commit('set', {type: 'routes', items: [] });
		commit('set', {type: 'isAuthed', items: false});
		commit('set', {type: 'isAdmin', items: false});
		commit('set', {type: 'isEditor', items: false});
		commit('set', {type: 'isSeo', items: false});
		commit('set', {type: 'isProg', items: false});
	},
}

const mutations = {
	set(state, {type, items}) {
		state[type] = items
	},
	reset(state, type) {
		state[type] = initialState()[type]
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
