import Vue from 'vue'
import Driver from 'driver.js/dist/driver.min.js'
import 'driver.js/dist/driver.min.css' // import driver.js css

let Guider = {}
Guider.install = function (Vue, options) {
	Vue.prototype.$guider = new Driver({
		animate          : true,
		opacity          : 0.75,
		padding          : 10,
		allowClose       : false,
		overlayClickNext : false,
		doneBtnText      : 'Готово',
		closeBtnText     : 'Закрыть',
		stageBackground  : '#ffffff',
		nextBtnText      : '>',
		prevBtnText      : '<',
		showButtons      : true,
		keyboardControl  : true,
	})
}
export default Guider
