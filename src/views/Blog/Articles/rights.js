export const Rights = {
	methods: {
		check_rights(article) {
			let rights = {
				create_new    : false,
				del           : false,
				edit          : false,
				publish       : false,
				redraft       : false,
				moderate      : false,
				seo           : false,
				edit_seo      : false,
				change_status : '',
			}

			let role = this.user.role

			if (role === 'seo') {
				rights.seo = true
			}

			if (role === 'seo' || role === 'admin') {
				rights.edit_seo = true
			}

			if (role !== 'admin' && role !== 'editor')
				return rights

			if (!article || !article.id) {
				rights.edit       = true
				rights.create_new = true
				return rights
			}

			let isAdmin = this.isAdmin;
			let isAuthor = false;
			if (article.id && parseInt(article.author) == parseInt(this.user.id))
				isAuthor = true;
			let isModerator = false;
			if (article.id && article.moderator && parseInt(article.moderator) == parseInt(this.user.id))
				isModerator = true;
			let isAuthorAdmin = false;
			if (article.id && article.authorRole === 'admin')
				isAuthorAdmin = true;


			if (article.status === 'draft' && isAuthor && !isAdmin){
				rights.edit          = true
				rights.moderate      = true
				rights.change_status = 'in_work'
			}
			if (article.status === 'draft' && isAdmin){
				rights.edit    = true
				rights.publish = true
			}

			if (article.status === 'in_work' && isAuthor){
				rights.edit          = true
				rights.moderate      = true
				rights.change_status = 'draft'
			}
			if (article.status === 'in_work' && isAdmin){
				rights.edit          = true
				rights.publish      = true
				rights.change_status = 'draft'
			}

			if (article.status === 'redraft' && isAuthor && !isAdmin){
				rights.edit          = true
				rights.moderate      = true
				rights.change_status = 'in_work'
			}
			if (article.status === 'redraft' && isAdmin){
				rights.edit          = true
				rights.publish       = true
				rights.change_status = 'in_work'
			}

			if (article.status === 'moderate' && isAdmin){
				rights.edit          = true
				rights.change_status = 'in_moderate'
			}
			if (article.status === 'moderate' && isAuthor){
				rights.edit          = true
				rights.change_status = 'in_work'
			}

			if (article.status === 'in_moderate' && isAdmin){
				rights.edit    = true
				rights.publish = true
				rights.redraft = true
			}

			if (article.status === 'publish' && isAdmin){
				rights.edit    = true
				rights.redraft = true
			}

			return rights
		}
	}
}
