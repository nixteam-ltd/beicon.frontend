import Vue            from 'vue'
import Router         from 'vue-router'

Vue.use(Router)

export const routerMap = [
	{
		path     : '/',
		redirect : '/dashboard',
		hidden   : true,
  },{
		path : '/dashboard',
		name : 'dashboard',
    component: () => import('@/views/Dashboard/Main'),
		meta : {
			title: 'Информация',
			icon: 'dashboard'
		}
	},{
  		path: '/blog',
  		name : 'blog',
      component: () => import('@/views/Blog/Main'),
  		meta: {
          	title: 'Блог',
          	roles: ['admin', 'editor', 'seo'],
          	icon: 'documentation'
      }
	},{
  		path: '/blog/category',
  		name : 'category',
      component: () => import('@/views/Blog/Category/Main'),
  		meta: {
          	title: 'Рубрики',
          	roles: ['admin', 'editor', 'seo'],
          	parent: 'blog'
      }
	},{
  		path: '/blog/tags',
  		name : 'tags',
  		component: () => import('@/views/Blog/Tags/Main'),
  		meta: {
          	title: 'Теги',
          	roles: ['admin', 'seo'],
          	parent: 'blog'
      }
	},{
      path: '/blog/persons',
      name : 'persons',
      component: () => import('@/views/Blog/Persons/Main'),
      meta: {
            title: 'Персоны',
            roles: ['admin', 'seo'],
            parent: 'blog'
        }
    },{
      path: '/blog/gallery',
      name : 'gallery',
      component: () => import('@/views/Blog/Gallery/Main'),
      meta: {
            title: 'Фотогалереи',
            roles: ['admin', 'editor'],
            parent: 'blog'
        },
    },{
      path: '/blog/rss',
      name : 'rss',
      component: () => import('@/views/Blog/RSS/Main'),
      meta: {
            title: 'RSS каналы',
            roles: ['admin'],
            parent: 'blog'
        }
    },{
      path      : '/blog/articles',
      name      : 'articles',
      component: () => import('@/views/Blog/Articles/Main'),
      meta      : {
          	title: 'Статьи',
          	roles: ['admin', 'seo', 'editor'],
          	parent: 'blog',
            important: true
        }
  	},{
      path      : '/blog/articles/item/',
      name      : 'add_article',
      hidden    : true,
      component: () => import('@/views/Blog/Articles/Item'),
      meta      : {
            title: 'Новая статья',
            roles: ['admin', 'seo', 'editor'],
            parent: 'articles'
        }
    },{
      path      : '/blog/articles/item/:id',
      name      : 'edit_article',
      hidden    : true,
      component: () => import('@/views/Blog/Articles/Item'),
      meta      : {
            title: 'Редактирование статьи',
            roles: ['admin', 'seo', 'editor'],
            parent: 'articles'
        }
    },{
      path: '/partners',
      name : 'partners',
      component: () => import('@/views/Partners/Main'),
      meta: {
            title: 'Материалы партнеров',
            roles: ['admin'],
            icon: 'hands-helping-solid'
      }
    },{
      path: '/pnews',
      name : 'pnews',
      component: () => import('@/views/Partners/PartnerNews/Main'),
      meta: {
            title: 'Новости партнеров',
            roles: ['admin'],
            parent: 'partners'
        },
    },{
      path: '/prss',
      name : 'prss',
      // component: () => import('@/views/PartnerNews/Main'),
      meta: {
            title: 'RSS партнеров',
            roles: ['admin'],
            parent: 'partners'
        },
    },{
      path: '/menu',
      name : 'menu',
      component: () => import('@/views/SiteMenu/Main'),
      meta: {
            title: 'Меню сайта',
            roles: ['admin'],
            icon: 'nested'
        },
    },{
  		path: '/marketings',
  		name : 'marketings',
      component: () => import('@/views/Marketings/Main'),
  		meta: {
          	title: 'Реклама',
          	roles: ['admin', 'programmer'],
          	icon: 'example'
        },
  	},{
  		path: '/pages',
  		name : 'pages',
      component: () => import('@/views/SitePages/Main'),
  		meta: {
          	title: 'Страницы сайта',
          	roles: ['admin'],
          	icon: 'list'
        },
  	},{
      path      : '/users',
      name      : 'users',
      component: () => import('@/views/Users/Main'),
  		meta: {
          	title: 'Пользователи системы',
          	roles: ['admin'],
          	icon: 'peoples'
        },
  	},{
  		path: '/tech',
  		name : 'tech',
      component: () => import('@/views/Technical/Main'),
  		meta: {
          	title: 'Технический раздел',
          	roles: ['admin', 'seo', 'programmer'],
          	icon: 'cog-solid'
        },
  	},{
      path: '/tech/seo',
      name : 'seo',
      component: () => import('@/views/Technical/Seo/Main'),
      meta: {
            title: 'SEO',
            roles: ['admin', 'seo'],
            parent: 'tech',
            important: true
        },
    },{
  		path: '/tech/social',
  		name : 'social',
      component: () => import('@/views/Technical/Social/Main'),
  		meta: {
          	title: 'Социальные сети',
          	roles: ['admin'],
          	parent: 'tech',
        },
  	},{
  		path: '/tech/service',
  		name : 'service',
      component: () => import('@/views/Technical/Service/Main'),
  		meta: {
          	title: 'Технические данные',
          	roles: ['admin', 'programmer', 'seo'],
          	parent: 'tech'
        },
  	},{
  		path: '/tech/subscribers',
  		name : 'subscribers',
      component: () => import('@/views/Technical/Subscribers/Main'),
  		meta: {
          	title: 'Подписки',
          	roles: ['admin'],
          	parent: 'tech'
        },
  	},
    {
      path: '/yacode',
      component: () => import('@/views/App/Yandex'),
      hidden: true
    },
    {
      path: '/404',
      component: () => import('@/views/App/Errors/404'),
      hidden: true
    },
    { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: routerMap
})
