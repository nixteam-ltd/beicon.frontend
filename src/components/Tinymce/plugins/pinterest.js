tinymce.PluginManager.add('pinterest', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('pinterest', {
        text: 'Pinterest',
        tooltip: 'Вставить пост из Pinterest',
        icon: false,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Вставить пост из Pinterest',
                body: [
                    {   type: 'textbox',
                        size: 70,
                        height: '100px',
                        name: 'pinterest',
                        label: 'Ссылка на пост',
                        tooltip: 'Например, https://www.pinterest.com/pin/99360735500167749/'
                    }
                ],
                onsubmit: function(e) {
                    var embedPost = e.data.pinterest;
                    var embedCode = '<p class="mceNonEditable"><a data-pin-width="large" data-pin-do="embedPin" href="' + embedPost + '">&nbsp;</a></p><br>';

                    var iframe = document.getElementById(editor.id + "_ifr");

                    tinyMCE.activeEditor.insertContent(embedCode);

                    const oldMyScript = iframe.contentDocument.getElementById('beicon-pinterest-restore');
                    if(oldMyScript)
                        oldMyScript.parentNode.removeChild(oldMyScript);

                    const myScript = document.createElement("script");
                    myScript.setAttribute("id", 'beicon-pinterest-restore');
                    myScript.setAttribute("type", "text/javascript");
                    const myScriptCode = `
                    window.rerenderPinterestWidgets = function() {
                        Object.keys(window).filter(function(k){ return ~k.indexOf("PIN") }).forEach(function(prop) {
                            delete window[prop];
                        });
                        var renderedPins = document.querySelectorAll('span[data-pin-id][data-pin-href][data-pin-log=embed_pin_large]')
                        var initialPin;
                        [].forEach.call(renderedPins, function(renderedPin) {
                            initialPin = document.createElement('a');
                            initialPin.setAttribute('data-pin-width', 'large');
                            initialPin.setAttribute('data-pin-do', 'embedPin');
                            initialPin.setAttribute('href', renderedPin.dataset.pinHref);
                            initialPin.innerHtml = '&nbsp';
                            renderedPin.parentElement.replaceChild(initialPin, renderedPin);
                        });
                        var oldPinterestScripts = document.querySelectorAll('script[src^="https://log.pinterest.com/?],script[src^=//assets.pinterest.com/js/]"');
                        [].forEach.call(oldPinterestScripts, function(oldPinterestScript) {
                          oldPinterestScript.parentNode.removeChild(oldPinterestScript);
                        });
                        var pInitScript = document.createElement("script");
                        pInitScript.setAttribute("id", 'beicon-pinterest-init');
                        pInitScript.setAttribute("src", '//assets.pinterest.com/js/pinit.js');
                        pInitScript.setAttribute("type", "text/javascript");
                        
                        document.getElementsByTagName('head')[0].appendChild(pInitScript);
                    }
                    rerenderPinterestWidgets();
                    `;
                    myScript.appendChild(document.createTextNode(myScriptCode));

                    iframe.contentDocument.getElementsByTagName('body')[0].appendChild(myScript);
                    iframe.contentWindow.rerenderPinterestWidgets();
                    window.rerenderPinterestWidgets = iframe.contentWindow.rerenderPinterestWidgets;
                }
            });
        }
    });
})
