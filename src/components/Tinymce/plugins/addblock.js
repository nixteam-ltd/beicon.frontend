function PluginBlock (editor, url) {
  // Add a button that opens a window
  editor.addButton('addblock', {
    text: 'Блок',
    tooltip: 'Вставить в статью блок',
    onclick: function() {
      // Open window
      editor.windowManager.open({
        title: 'Добавление блока',
        body: [
          {
            type: 'listbox',
            name: 'addblock',
            size: 'large',
            label:'Выберите блок для вставки',
            values:[
              {value:'PRODUCT_BLOCK1', text:'Товары (слайдер)'},
              {value:'PRODUCT_BLOCK2', text:'Товары (список)'},
              {value:'BANNER_BLOCK', text:'Баннер'},
            ]}
        ],
        onsubmit: function(e) {
          // Insert content when the window form is submitted
          editor.insertContent('<p>[[' + e.data.addblock + ']]</p><p></p>');
        }
      });
    }
  });

  return {
    getMetadata: function () {
      return  {
        name: "addblock plugin",
      };
    }
  };
}

tinymce.create('tinymce.plugins.AddBlock', {
  init: function (editor, url) {
    return new PluginBlock(editor, url)
  },
})

tinymce.PluginManager.add('addblock', tinymce.plugins.AddBlock);

