tinymce.PluginManager.add('gallery', function(editor, url) {
   var self = this, button;

   function getValues() {
      return editor.settings.gallery_array;
   }
   // Add a button that opens a window
   editor.addButton('gallery', {
      type: 'listbox',
      text: 'Галерея',
      tooltip: 'Вставить в статью галерею',
      values: getValues(),
      onselect: function(e) {
        // console.log( e.control.settings.id );

         //insert key
         editor.insertContent('{{GALLERY=' + e.control.settings.id + '}}');

         //reset selected value
         this.value(null);
      },
      onPostRender: function() {
         button = this;
      },
   });

   self.refresh = function() {
      //remove existing menu if it is already rendered
      if(button.menu){
         button.menu.remove();
         button.menu = null;
      }

      button.settings.values = button.state.data.menu = getValues();
   };
});

