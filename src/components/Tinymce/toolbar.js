// Here is a list of the toolbar
// Detail list see https://www.tinymce.com/docs/advanced/editor-control-identifiers/#toolbarcontrols

const toolbar = ['bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | subscript superscript | forecolor backcolor | removeformat undo redo | code spellchecker fullscreen', 'formatselect hr bullist numlist insertdatetime  table  link blockquote image media instagram pinterest gallery addblock']

export default toolbar
