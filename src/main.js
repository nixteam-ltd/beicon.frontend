// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue              from 'vue'
import App              from './views/App'
import router           from './router/router'
import store            from './store/store'

import ElementUI from 'element-ui'

import 'normalize.css/normalize.css'
import '@/assets/styles/webix.css' // webix css
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/styles/index.scss' // global css

import locale from 'element-ui/lib/locale/lang/ru-RU'
Vue.use(ElementUI, { locale });

import moment           from 'moment'
import 'moment/locale/ru'
Vue.use(require('vue-moment'));
Vue.prototype.moment = moment

import './directives/icons'

import VueResource from 'vue-resource'
Vue.use(VueResource)

import Guider from './plugins/guider'
Vue.use(Guider)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
