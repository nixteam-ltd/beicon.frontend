import $            from 'jquery'

export const NixTable = {

    data(){
        return {
            delVisible        : false,
            showSaveBtn       : false,
            showSeoBtn        : false,
            dialogFormVisible : false,
            formData          : {},
        }
    },

	computed: {
        tableHeight(){
        	return $(window).height() - 170
        },

        formTitle(){
            if (!this.currentRow || !this.currentRow.id)
                return this.formTitles.new_rec
            else
                return this.formTitles.edit_rec + ' "' + this.currentRow.name + '"'
        },
    },

	methods: {
		inputFilter(createElement, { column, store }) {

            $('.caret-wrapper').hide();
            column.sortable = "custom"

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}, [
                            createElement('i', {class: "sort-caret ascending"}, ),
                            createElement('i', {class: "sort-caret descending"}, ),
                        ]),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-input', {
                        props: {
                            size      : 'small',
                            clearable : true,
                            value     : this.filters[column.property].value
                        },
                        on: {
                            change: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            },
                            input: (value) => {
                                this.filters[column.property].value = value
                            }
                        },
                        nativeOn: {
                            click(e){
                                e.stopPropagation();
                            },
                            input(e){
                                e.stopPropagation();
                            }
                        }
                    })
                ]),
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;

        },

        inputNosortFilter(createElement, { column, store }) {

            $('.caret-wrapper').hide();

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-input', {
                        props: {
                            size      : 'small',
                            clearable : true,
                            value     : this.filters[column.property].value
                        },
                        on: {
                            change: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            },
                            input: (value) => {
                                this.filters[column.property].value = value
                            }
                        },
                        nativeOn: {
                            click(e){
                                e.stopPropagation();
                            }
                        }
                    })
                ]),
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;

        },

        noFilter(createElement, { column, store }) {

			$('.caret-wrapper').hide();
			column.sortable = "custom"

			let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                	createElement('div', {class: 'nixtbl-th-title'}, column.label),
                	createElement('div', {class:'nixtbl-th-sort'}, [
                		createElement('span', {class:"caret-wrapper show"}, [
                			createElement('i', {class: "sort-caret ascending"}, ),
                			createElement('i', {class: "sort-caret descending"}, ),
                		]),
                	]),
                ]),

                createElement('div', {class: 'nixtbl-th', style: 'height: 20px;'}, ' '),
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;

		},

        noNosortFilter(createElement, { column, store }) {

            $('.caret-wrapper').hide();

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th', style: 'height: 35px;'}, ' '),
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;

        },

		selectFilter(createElement, { column, store }) {

			$('.caret-wrapper').hide();
			column.sortable = "custom"

            let options = this.collectValues(column.property)
            // options.unshift({value: 'false', key: 'false', label:'- пусто -'})

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                	createElement('div', {class: 'nixtbl-th-title'}, column.label),
                	createElement('div', {class:'nixtbl-th-sort'}, [
                		createElement('span', {class:"caret-wrapper show"}, [
                			createElement('i', {class: "sort-caret ascending"}, ),
                			createElement('i', {class: "sort-caret descending"}, ),
                		]),
                	]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-select', {
                        props: {
                            multiple  : false,
                            clearable  : true,
                            placeholder: '',
                            'collapse-tags' : true,
                            size      : 'small',
                            value     : this.filters[column.property].value
                        },
                        on: {
                            input: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            }
                        }
                    }, options.map((item) => {
                        return createElement('el-option', {props: item});
                    }))
                ])
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;
        },

        selectNosortFilter(createElement, { column, store }) {

            $('.caret-wrapper').hide();

            let options = this.collectValues(column.property)
            // options.unshift({value: 'false', key: 'false', label:'- пусто -'})

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-select', {
                        props: {
                            multiple  : false,
                            clearable  : true,
                            placeholder: '',
                            'collapse-tags' : true,
                            size      : 'small',
                            value     : this.filters[column.property].value
                        },
                        on: {
                            input: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            }
                        }
                    }, options.map((item) => {
                        return createElement('el-option', {props: item});
                    }))
                ])
            ])

            element.fnScopeId = this.$options._scopeId;
            element.fnContext = this;

            $('.caret-wrapper.show').show();

            return element;
        },

        dateFilter(createElement, { column, store }) {
            $('.caret-wrapper').hide();
            column.sortable = true

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}, [
                            createElement('i', {class: "sort-caret ascending"}, ),
                            createElement('i', {class: "sort-caret descending"}, ),
                        ]),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-date-picker', {
                        props: {
                            value  : this.filters[column.property].value,
                            type   : "date",
                            format : "dd.MM.yyyy",
                            size   : 'small',
                            'value-format': "yyyy-MM-dd"
                        },
                        on: {
                            input: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            }
                        },
                        nativeOn: {
                            click(e){
                                e.stopPropagation();
                            }
                        }
                    })
                ])
            ])

            return element;
        },

        dateNosortFilter(createElement, { column, store }) {
            $('.caret-wrapper').hide();

            let element = createElement('div', {class : 'nixtbl-th-cell'}, [
                createElement('div', {class: 'nixtbl-th'},[
                    createElement('div', {class: 'nixtbl-th-title'}, column.label),
                    createElement('div', {class:'nixtbl-th-sort'}, [
                        createElement('span', {class:"caret-wrapper show"}),
                    ]),
                ]),

                createElement('div', {class: 'nixtbl-th'},[
                    createElement('el-date-picker', {
                        props: {
                            value  : this.filters[column.property].value,
                            type   : "date",
                            format : "dd.MM.yyyy",
                            size   : 'small',
                            'value-format': "yyyy-MM-dd"
                        },
                        on: {
                            input: (value) => {
                                this.filters[column.property].value = value
                                this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
                                this.$store.commit(this.module + '/set', {type: 'currentPage', items:1});
                                this.$store.dispatch(this.module + '/getList');
                            }
                        },
                        nativeOn: {
                            click(e){
                                e.stopPropagation();
                            }
                        }
                    })
                ])
            ])

            return element;
        },

        collectValues(id) {
            var values = [];

            if (this.sFilter && this.sFilter[id]) {
                this.sFilter[id].map((item) => {
                    values.push({ value:item.id, label:item.name, key:item.id });
                })
            }

            else {
                var checks = {};
                var bool = false;

                this.list.map((item) => {
                    if (typeof item[id] === 'boolean') {
                        bool = true;
                    }
                    else {
                        var test = (item && item[id]) ?  item[id] : '';
                        if (test && !checks[test]) {
                            checks[test] = true;
                            values.push({ value:item[id], label:item[id], key:item[id] });
                        }
                    }
                })

                if (bool) {
                    values = [
                        {value: 'true', label: 'да', key: 'true'},
                        {value: 'false', label: 'нет', key: 'false'},
                    ];
                }
            }

            values.sort(function(a,b){ return a.value > b.value ? 1 : -1;  });

            return values;
        },

        handleSortChange(column) {
            var dir = (column.order === 'ascending') ? 'asc' : 'desc';
            this.$store.commit(this.module + '/set', {type: 'sorting', items:{prop: column.prop, order: column.order, dir: dir}});
            this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
            this.$store.dispatch(this.module + '/getList');
        },

        changePage(newPage){
            this.$store.commit(this.module + '/set', {type: 'currentPage', items:newPage});
            this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
            this.$store.dispatch(this.module + '/getList');
        },

        handleCurrentChange(val){
            if (val) this.$store.commit(this.module + '/set', {type: 'currentRow', items:val});
        },

        showBtn(rowId) {
            if (!this.currentRow || !this.currentRow.id)
                return false
            if (this.currentRow.id == rowId)
                return true
            return false
        },

        addRecord(){
            this.dialogFormVisible = true
            if (this.$refs.Editor) this.$refs.Editor.clearValidate()
            this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}})
            this.formData = Object.assign({}, this.formInitData)
        },
        editRecord(){
            this.dialogFormVisible = true
            if (this.$refs.Editor) this.$refs.Editor.clearValidate()
            this.formData = Object.assign({}, this.currentRow)
        },
        editRecordInline(record){
            this.$store.dispatch(this.module + '/updateItem', record)
        },
        deleteRecord(id){
            this.$store.commit(this.module + '/set', {type: 'currentRow', items:{}});
            this.formData = Object.assign({}, this.formInitData)
            this.$store.dispatch(this.module + '/deleteItem', id);
            this.delVisible = false
        },
        cancelEdit(){
            this.dialogFormVisible = false;
        },

        saveEdit(){
            var self = this;
            self.$refs.Editor.validate((valid) => {
                if (valid) {
                    this.dialogFormVisible = false
                    if (self.formData.id)
                        this.$store.dispatch(this.module + '/updateItem', self.formData)
                    else
                        this.$store.dispatch(this.module + '/addItem', self.formData);

                } else {
                    return false;
                }
            });
        },

        tabSelected(clicked){
            if (clicked._props.name === 'seo') {
                this.showSaveBtn = false;
                this.showSeoBtn = true;
                this.$store.dispatch(this.module + '/getSEO', this.currentRow.id);
            }
            else if (clicked._props.name === 'common') {
                this.showSaveBtn = true;
                this.showSeoBtn = false;
            }
        },

        saveSeo(){
            if (this.seo.id) {
                this.$store.dispatch('app/edit_seo', this.seo);
            }
            else {
                this.$store.dispatch('app/add_seo', this.seo);
            }
            this.dialogFormVisible = false
        }
	},

    watch: {
        currentRow(){
            if (this.currentRow && this.currentRow.id) {
                this.$refs.Tbl.setCurrentRow(this.currentRow)
            }
            else {
                this.$store.commit(this.module + '/set', {type: 'currentRow', items:this.formInit});
                this.$refs.Tbl.setCurrentRow()
            }
        },
        dialogFormVisible(val) {
            if (val) {
                let self = this
                setTimeout(function(){
                    if (self.$refs.EditorTabs && self.$refs.EditorTabs.currentName === 'common') {
                        self.showSaveBtn = true;
                        self.showSeoBtn = false;
                    }
                    else if (self.$refs.EditorTabs && self.$refs.EditorTabs.currentName === 'seo') {
                        self.showSaveBtn = false;
                        self.showSeoBtn = true;
                        self.$store.dispatch(self.module + '/getSEO', self.currentRow.id);
                    }
                    else {
                        self.showSaveBtn = false;
                        self.showSeoBtn = false;
                    }

                }, 300);
            }
        }
    },

    mounted () {
        this.$store.dispatch(this.module + '/getList');
    },
    destroyed(){
        this.$store.commit(this.module + '/set', {type: 'list', items:[]});
    },

}
